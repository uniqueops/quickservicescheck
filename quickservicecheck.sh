#!/bin/bash
# SSH Command: bash <(curl -ks https://gitlab.com/uniqueops/quickservicescheck/raw/master/quickservicecheck.sh)
# Description: This script will check the following services on the server to see if they are up. (named, cPanel, Apache, MySQL, FTP, Exim, LFD, Dovecot or Courier)

green="\E[1;32m"
RESET="\E[0;00m"
yellow="\e[1;33m"
red='\E[1;31m'
varok='OK'
vardown='DOWN'
varno='NO'
servicenum=0
CENTOS7FLAG=''
### Look for CloudLinux ###
if [[ -n `uname -r | grep lve` ]]; then
    CENTOS7CHECK=$(cat /etc/redhat-release | awk '{print $3}' | cut -d . -f1 | egrep [0-9]+)
else
    CENTOS7CHECK=$(cat /etc/redhat-release | awk '{print $4}' | cut -d . -f1 | egrep [0-9]+)
fi
if [[ "$CENTOS7CHECK" -ge 7 ]]; then
  CENTOS7FLAG=1
fi
  

############  Named #############
#httpdflag=$(pgrep named | wc -l)
namedflag=$(ps faux | grep -v grep | grep named | wc -l)
if [[ "$namedflag" == 0 ]]
then
        printf "%-8s $red %-10s\n$RESET" "Named" "$vardown"
else
        printf "%-8s $green %-10s\n$RESET" "Named" "$varok"
fi

############  Apache #############
httpdflag=$(ps faux | grep httpd | grep -v grep | grep root | wc -l)
if [[ "$httpdflag" == 0 ]]
then
        printf "%-8s $red %-10s\n$RESET" "Apache" "$vardown"
else
        printf "%-8s $green %-10s\n$RESET" "Apache" "$varok"
fi

############ Nginx ################
nginxflag=$(ps faux | grep nginx | grep -v grep | grep root | wc -l);
x=$(grep -o "Listen 0.0.0.0:8081" /etc/httpd/conf/httpd.conf);
if [[ $x == "Listen 0.0.0.0:8081" ]]; then
        if [ "$nginxflag" == 0 ]; then
                printf "%-8s $red %-10s\n$RESET" "Nginx" "$vardown"
        else
                printf "%-8s $green %-10s\n$RESET" "Nginx" "$varok"
        fi
fi

############ Litespeed ##############
if [[ -f /etc/init.d/lsws ]]; then
  lspeed=$(/etc/init.d/lsws status | grep -io "running" | tr '[:upper:]' '[:lower:]' )
  if [[ $lspeed == "running" ]]; then
    printf "%-8s $green %-10s\n$RESET" "Litespeed" "$varok"
  else
    printf "%-8s $red %-10s\n$RESET" "Litespeed" "$vardown"
  fi
fi
    
############  cPanel #############
cpanel=$(/etc/init.d/cpanel status 2>/dev/null | grep -io "running" | tr '[:upper:]' '[:lower:]')
cproc=$(ps faux | grep cpsrvd | grep -v grep | wc -l);
z=running
if [[ $cpanel == $z* ]] || [[ $cproc -ge 1 ]]; then
        printf "%-8s $green %-10s\n$RESET" "cPanel" "$varok"
else
        printf "%-8s $red %-10s\n$RESET" "cPanel" "$vardown"
fi

############  MySQL #############
mysql=$(/etc/init.d/mysql status 2>/dev/null | grep -io "running" | tr '[:upper:]' '[:lower:]')
mproc=$(ps faux | grep mysql | grep -v grep | wc -l);
z=running
if [[ $mproc -ge 1 ]]; then
        printf "%-8s $green %-10s\n$RESET" "MySQL" "$varok"

else
        printf "%-8s $red %-10s\n$RESET" "MySQL" "$vardown"
fi

############  FTP #############
ftpserver=$(/scripts/setupftpserver --current | awk {'print $5'});
if [[ $ftpserver == "pure-ftpd" ]]; then
        #ftpflag=$(/etc/init.d/pure-ftpd status 2>/dev/null | grep -io "running" | tr '[:upper:]' '[:lower:]');
        ftpflag=$(pgrep pure-ftpd | wc -l)
        if [[ $ftpflag -ge 1 ]]
        then
                printf "%-8s $green %-10s\n$RESET" "FTP" "$varok"
        else
                printf "%-8s $red %-10s\n$RESET" "FTP" "$vardown"

        fi
#It's proftp
elif [[ $ftpserver == "proftpd" ]]; then
        #ftpflag=$(/etc/init.d/proftpd status 2>/dev/null | grep -io "running" | tr '[:upper:]' '[:lower:]');
        ftpflag=$(pgrep ftpd | wc -l)
        if [[ $ftpflag -ge 1 ]]; then
                printf "%-8s $green %-10s\n$RESET" "FTP" "$varok"
        else
                printf "%-8s $red %-10s\n$RESET" "FTP" "$vardown"
        fi
else
        echo -e "$red No FTP server detected $RESET"
fi


############  Exim #############
#exim=$(/etc/init.d/exim status 2>/dev/null | grep -io "running" | tr '[:upper:]' '[:lower:]')
exim=$(pgrep exim | wc -l)
z=running
if [[ $exim -ge 1 ]]; then
        printf "%-8s $green %-10s\n$RESET" "EXIM" "$varok"
else
        printf "%-8s $red %-10s\n$RESET" "EXIM" "$vardown"
fi

############  LFD #############
if [[ $CENTOS7FLAG = 1 ]]; then
        if [[ -n `systemctl status lfd 2>/dev/null | grep active` ]]; then
                printf "%-8s $green %-10s\n$RESET" "LFD" "$varok"
        else
                printf "%-8s $red %-10s\n$RESET" "LFD" "$vardown"
        fi
else
        if [[ ! -f /etc/init.d/lfd ]]; then
                echo -e "$red CSF/LFD is not installed $RESET"
        else
                lfd=$(/etc/init.d/lfd status 2>/dev/null | grep -io "running" | tr '[:upper:]' '[:lower:]');
                if [[ $lfd == "running" ]]; then
                        printf "%-8s $green %-10s\n$RESET" "LFD" "$varok"
                else
                        printf "%-8s $red %-10s\n$RESET" "LFD" "$vardown"
        fi
fi
fi

############  Dovecot or Courier #############
mailserver=$(/scripts/setupmailserver --current | awk 'FNR==1' | awk {'print $4'});
dov=$(ps fuax | grep dovecot | grep -v grep)
if [[ $mailserver == dovecot && -n $dov ]]
then
        printf "%-8s $green %-10s\n$RESET" "Dovecot" "$varok"
elif [[  $mailserver == dovecot && -z $dov ]]
then
        printf "%-8s $red %-10s\n$RESET" "Dovecot" "$vardown"
fi

cour=$(ps fuax | grep courier-imap | grep -v grep)
cour2=$(ps fuax | grep courier-auth | grep -v grep)
if [[ $mailserver == courier && -n $cour && -n $cour2 ]]
then
        printf "%-8s $green %-10s\n$RESET" "Courier" "$varok"
elif [[ $mailserver == courier && -z $cour ]]
then
        printf "%-8s $red%-10s\n$RESET" "Courier" "$vardown"
elif [[ $mailserver == courier && -z $cour2 ]]
then
        printf "%-8s $red%-10s\n$RESET" "Courier" "$vardown"
fi
############  Crond #############
cron=$(ps faux | grep crond | grep -v grep | wc -l);
if [[ $cron -ge 1 ]]; then
        printf "%-8s $green %-10s\n$RESET" "Crond" "$varok"

else
        printf "%-8s $red %-10s\n$RESET" "Crond" "$vardown"
fi
############# CpHulk ################
varcp='Enabled'
hulksmash=$(pgrep hulk | wc -l);
if [[ $hulksmash -ge 1 ]]; then
        printf "%-8s $red %-10s\n$RESET" "cPHulk" "$varcp"
fi

############# Common problems check ################
#### Checks if more than 1 parent process for apache is running
APACHEGRACE=$(ps faux | grep "/usr/local/apache/bin/httpd -k graceful" | grep -v grep | grep root | awk '{print $1, $11}' | wc -l);
APACHE2=$(ps faux | grep "/usr/local/apache/bin/httpd" | grep -v grep | grep -c root | awk '{print $1, $11}');
if [[ $APACHE2 -gt 1 ]]; then
        printf "%-8s $red %-10s\n$RESET" "Apache" "Multiple parent processes detected!"
fi
if [[ $APACHEGRACE -ge 1 ]]; then
        printf "%-8s $red %-10s\n$RESET" "Apache" "Graceful restart process detected!"
fi